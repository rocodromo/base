<?php
set_time_limit(0);
require_once 'lib/base/common.php';
require_once 'lib/base/Base.php';
require_once 'storage/Base.php';
require_once 'lib/base/Queue.php';
require_once 'route_map.php';

Base::registerAutoloader();

$queue = new QueueRunner($map);
$queue->run();
