<?php
require_once 'lib/base/common.php';
require_once 'lib/base/Base.php';
require_once 'lib/base/Queue.php';
require_once 'route_map.php';

Base::registerAutoloader();

$api = new ApiRunner($map);
$api->run();
