<?php
class BaseQueueFilesStore extends BaseStore {
  const
    COLLECTION = 'fb_campaign_queue_files',
    MODEL = 'BaseQueueFileModel';

  public function __construct() {
    parent::__construct(self::COLLECTION, self::MODEL);
  }
}

class BaseQueueFileModel extends BaseModel {
  public function schema() {
    return [];
  }
}

class BaseQueueStore extends BaseStore {
  const
    COLLECTION = 'queue',
    MODEL = 'BaseQueueMessage';
}

class BaseQueueMessage extends BaseModel {
  public function schema() {
    return array_fill_keys([
      '_id',
      'route',
      'method',
      'params',
      'files',
      'scheduled_at',
      'attempts',
    ], BaseModel::FIELD);
  }

  public function __construct() {
    $args = func_get_args();
    if (count($args) == 1 && is_array($args[0])) {
      return parent::__construct($args[0]);
    } elseif (count($args) >= 3) {
      $this->set('route', $args[0]);
      $this->set('method', isset($args[1]) ? $args[1] : 'GET');
      $this->set(
        'params', isset($args[2]) && is_array($args[2]) ? $args[2] : []);

      $this->set('files', isset($args[3]) && is_array($args[3]) ?
        $this->prepareFiles($args[3]) : []);
      $this->set('scheduled_at', isset($args[4]) && is_numeric($args[4]) ?
        $args[4] : time());
      $this->set('attempts', 0);
    } else {
      throw new Exception('Invalid number of arguments provided.');
      return null;
    }

    $method = strtoupper($args[1]);
    switch ($method) {
      case BaseQueue::GET:
      case BaseQueue::PUT:
      case BaseQueue::POST:
      case BaseQueue::UPDATE:
      case BaseQueue::DELETE:
      case BaseQueue::HEAD:
      case BaseQueue::OPTIONS:
        $this->set('method', $method);
        break;
      default:
        throw new Exception('Invalid method specified.');
        return null;
    }
  }

  protected function prepareFiles($files) {
    foreach ($files as &$file) {
      $path = realpath('data/queue/' . uniqid());
      move_uploaded_file($file['tmp_name'], $path);
      $file['tmp_name'] = $path;
    }

    return $files;
  }
  public function route() {return $this->get('route');}
  public function method() {return $this->get('method');}
  public function params() {return $this->get('params');}
  public function files() {return $this->get('files');}
  public function incrementAttemptsCount() {
    $this->set('attempts', $this->get('attempts') + 1);
  }
  public function param($key) {
    return idx($this->get('params'), $key, null);}

  public function scheduledAt($unix_timestamp = null) {
    if (!$unix_timestamp) {
      return $this->get('scheduled_at');
    }

    $this->set('scheduled_at', $unix_timestamp);
  }
}

class BaseQueue {
  const
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    UPDATE = 'UPDATE',
    HEAD = 'HEAD',
    OPTIONS = 'OPTIONS';

   public static function q(
    $route, $method = BaseQueue::POST, $params = [], $scheduled_at = null) {
    if (!is_array($params)) {
      throw new Exception('Invalid params supplied to BaseQueue.');
      return;
    }

    $message = new BaseQueueMessage(
      $route, $method, $params, null, $scheduled_at);

    $queue = new BaseQueueStore();
    $queue->save($message);
    return $message->getID();
  }
}

class QueueRunner extends ApiRunner {
  const MAX_ATTEMPTS = 1;
  protected $store;
  protected function getPathInfo() {
    return $this->message->route();
  }

  protected function getMessage() {
    return $this->queue->findOne([
      'attempts' => ['$lt' => self::MAX_ATTEMPTS],
      'scheduled_at' => ['$lte' => time()]]);
  }

  protected function getRequestMethod() {
    return $this->message->method();
  }

  protected function getParams() {
    return array_merge($this->params, $this->message->params());
  }

  protected function getFiles() {
    return [];
  }

  protected function canAccessRestrictedEndpoints() {
    return true;
  }

  public function __construct($map) {
    l('Queue init...');
    parent::__construct($map);
    $this->queue = new BaseQueueStore();
  }

  public function run() {
    l('Queue running.');
    while (1) {
      $this->message = $this->getMessage();
      if (!$this->message) {
        continue;
      }

      try {
        l($this->getRequestMethod() . ' ' . $this->getPathInfo());
        ob_start();
        $controller = parent::run();
        $output = ob_get_contents();
        $message = sprintf(
          '%s %s done: %s',
          $this->getRequestMethod(),
          $this->getPathInfo(),
          $output);
        l($message);

        ob_end_clean();
      } catch (Exception $e) {
        $e = sprintf(
          '%s %s failed: %s',
          $this->getRequestMethod(),
          $this->getPathInfo(),
          $e->getMessage());
        l($e);
      }

      if (isset($controller) && $controller->done()) {
        $this->queue->remove($this->message);
        $this->message = null;
      } else {
        $this->message->incrementAttemptsCount();
        $this->queue->save($this->message);
      }
    }
  }
}
