<?php
class BaseTest {
  protected
    $scenario,
    $setupFn,
    $teardownFn,
    $tests,
    $total,
    $pass,
    $fail;

  public function __construct($scenario = 'Test') {
    $this->scenario = $scenario;
    $this->tests = [];
    $this->pass = 0;
    $this->fail = 0;
  }

  public function test($name, callable $test) {
    $this->tests[$name] = $test;
    $this->total++;
  }

  public function setup(callable $callable) {
    $this->setupFn = $callable;
  }

  public function teardown(callable $callable) {
    $this->teardownFn = $callable;
  }

  public function run() {
    $start_time = microtime(true);
    if ($this->scenario) {
      echo '[START] ' . $this->scenario . PHP_EOL;
    }
    $early_fail = false;
    if (is_callable($this->setupFn)) {
      echo '[START] Setup...' . PHP_EOL;
      $res = call_user_func($this->setupFn);
      if ($res === false) {
        $early_fail = true;
        echo 'Setup failed. Will not run test cases.' . PHP_EOL;
      }
    }

    if (!$early_fail) {
      foreach ($this->tests as $name => $callable) {
        if ($callable() === false) {
          echo sprintf('[FAIL] %s%s', $name, PHP_EOL);
          $this->fail++;
        } else {
          echo sprintf('[PASS] %s%s', $name, PHP_EOL);
          $this->pass++;
        }
      }

      if (is_callable($this->teardownFn)) {
        echo '[END] Teardown...' . PHP_EOL;
        call_user_func($this->teardownFn);
      }
    }

    $end_time = microtime(true);
    $total_time = $end_time - $start_time;
    $total_time = round($total_time, 4);

    $results = sprintf('Total: %d, pass: %d, fail: %d',
      $this->total, $this->pass, $this->fail);

    if ($this->scenario) {
      echo '[END] ' . $results . ' (' . $total_time . 's)' .
        PHP_EOL . PHP_EOL;
    }
  }
}

class BaseTestCase {
  protected $tests = [];

  public function tests() {
    return $this->tests;
  }

  public function addTest(BaseTest $test) {
    $this->tests[] = $test;
  }

  protected function checkResponseStatus($response) {
    return stripos($response->headers['Status'], '200 OK') !== false;
  }

  protected function parseResponse($response) {
    if (isset($response->body)) {
      return is_string($response) ?
        json_decode($response->body) : $response->body;
    }

    return null;
  }

  protected function success($response) {
    if (is_a($response, 'CurlResponse')) {
      $response = $this->parseResponse($response);
    }

    return $response != null && isset($response->success) &&
      $response->success == true;
  }

}
