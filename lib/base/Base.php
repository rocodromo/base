<?php
class BaseController {
  const SKIP_PARAM_VALIDATION = 'SKIP_PARAM_VALIDATION';
  protected
    $app,
    $params,
    $restricted,
    $path;

  public final function __construct(
    $route = null,
    $params = [],
    $files = [],
    $can_access_restricted_endpoints = false) {
    $this->restricted = false;
    $this->params = [];
    $this->path = $route;

    $required_params = $this->requiredParams();
    $optional_params = $this->optionalParams();

    if (!(is_array($required_params) ||
      $required_params == self::SKIP_PARAM_VALIDATION) ||
      !(is_array($optional_params) ||
            $optional_params == self::SKIP_PARAM_VALIDATION)) {
      throw new Exception('Invalid parameters supplied.');
      return $this;
    }

    $p = array_merge($_GET, $_POST, $_FILES, $params, $files);

    if ($required_params != self::SKIP_PARAM_VALIDATION) {
      foreach ($required_params as $param) {
        if (!array_key_exists($param, $p)) {
          $this->error('A required parameter is missing: ' . $param);
          return $this;
        }

        $this->params[$param] = $p[$param];
      }
    }

    if ($optional_params != self::SKIP_PARAM_VALIDATION) {
      foreach ($optional_params as $key => $default_value) {
        $this->params[$key] =
          array_key_exists($key, $p) ? $p[$key] : $default_value;
      }
    }

    if ($required_params == self::SKIP_PARAM_VALIDATION &&
      $optional_params == self::SKIP_PARAM_VALIDATION
      && empty($this->params)) {
      $this->params = $p;
    }

    try {
      $this->init();
      if ($this->restricted && !$can_access_restricted_endpoints) {
        l('Trying to access restricted endpoint', $this->path);
        throw new Exception('Endpoint not found: ' . $this->path, 404);
        return $this;
      }
      $error = '';
      foreach ($this->genFlow($error) as $res) {
        if ($res === false) {
          throw new Exception($error);
          return $this;
        }
        $error = null;
      }
      $this->success = true;
      $this->render();
    } catch (Exception $e) {
      $this->success = false;
      $code = $e->getCode() ? $e->getCode() : 400;
      $this->renderError($e, $code);
    }

    return $this;
  }

  // Use this method in your init() to map this controller to a private
  // endpoint.
  protected final function restricted() {
    $this->restricted = true;
  }

  // Controllers can override this and use it as a constructor.
  protected function init() {}

  // An array of parameters that must be present (either as GET, POST
  // or FILES) in the request body. If a parameter is missing, the
  // controller will not throw an expection and will not execute.
  protected function requiredParams() {return self::SKIP_PARAM_VALIDATION;}

  // An associative array of optional params (GET, POST or FILES), in which
  // the key is the parameter name, and the value is its default value in case
  // the parameter is not present in the request.
  // Other extra parameters coming from the request will be discarded.
  protected function optionalParams() {return self::SKIP_PARAM_VALIDATION;}

  // Controllers need to implement this in order to generate their flow.
  // Yielding a strict false will stop the controller and generate an
  // exception, which is useful to halt the execution flow when an error
  // occurs. An optional $error can be specified if you need to set the
  // exception message.
  protected function genFlow(&$error = null) {return [];}

  // Called when the controller executes getFlow with success. Override this
  // method when you need to send extra field in your response.
  protected function render() {$this->success();}

  // Called when the controller does not execute genFlow with success. Override
  // this method when you need to send a custom error response.
  protected function renderError(Exception $e) {
    $this->error($e->getMessage());
  }

  protected final function param($key, $default_value = null) {
    return idx($this->params, $key, $default_value);
  }

  protected final function env($key) {
    return idx($_SERVER, $key, null);
  }

  protected final function out($msg, $http_status = null) {
    // if (!headers_sent()) {
      header('Access-Control-Allow-Origin: *');
      header('Content-type: application/json; charset: utf-8');

      if ($http_status !== null) {
        header(' ', true, $http_status);
      }

    // }
    echo json_encode($msg);
  }

  protected final function success($data = null, $http_status = 200) {
    $payload = ['success' => true];

    if ($data) {
      $payload['data'] = $data;
    }

    $this->out($payload, $http_status);
  }

  protected final function error($code = true, $http_status = 400) {
    $this->out(['success' => false, 'error' => $code], $http_status);
  }

  public final function done() {
    return $this->success;
  }
}

class BaseNotFoundController extends BaseController {
  protected function requiredParams() {
    return ['path_info'];
  }
  public function render() {
    $this->error('Endpoint not found: ' . $this->param('path_info'), 404);
  }
}

class ApiRunner {
  protected
    $map,
    $pathInfo,
    $params,
    $paramNames;

  public function __construct($map) {
    $this->map = $map;
    $this->paramNames = [];
    $this->params = [];
  }

  protected function getPathInfo() {
    if ($this->pathInfo) {
      return $this->pathInfo;
    }

    if (strpos($_SERVER['REQUEST_URI'], $_SERVER['SCRIPT_NAME']) === 0) {
        $script_name = $_SERVER['SCRIPT_NAME']; //Without URL rewrite
    } else {
        $script_name = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'])); //With URL rewrite
    }

    $this->pathInfo = substr_replace(
      $_SERVER['REQUEST_URI'], '', 0, strlen($script_name));
    if (strpos($this->pathInfo, '?') !== false) {
        $this->pathInfo = substr_replace(
          $this->pathInfo, '', strpos($this->pathInfo, '?'));
    }

    $script_name = rtrim($script_name, '/');
    $this->pathInfo = '/' . ltrim($this->pathInfo, '/');


    return $this->pathInfo;
  }

  public function matches($resourceUri, $pattern)
  {
      //Convert URL params into regex patterns, construct a regex for this route, init params
      $patternAsRegex = preg_replace_callback('#:([\w]+)\+?#', [$this, 'matchesCallback'],
          str_replace(')', ')?', (string) $pattern));

      if (substr($pattern, -1) === '/') {
          $patternAsRegex .= '?';
      }

      //Cache URL params' names and values if this route matches the current HTTP request
      if (!preg_match('#^' . $patternAsRegex . '$#', $resourceUri, $paramValues)) {
          return false;
      }
      foreach ($this->paramNames as $name) {
          if (isset($paramValues[$name])) {
              if (isset($this->paramNamesPath[ $name ])) {
                  $this->params[$name] = explode('/', urldecode($paramValues[$name]));
              } else {
                  $this->params[$name] = urldecode($paramValues[$name]);
              }
          }
      }

      return true;
  }

  /**
   * Convert a URL parameter (e.g. ":id", ":id+") into a regular expression
   * @param  array    URL parameters
   * @return string   Regular expression for URL parameter
   */
  protected function matchesCallback($m)
  {
      $this->paramNames[] = $m[1];
      if (isset($this->conditions[ $m[1] ])) {
          return '(?P<' . $m[1] . '>' . $this->conditions[ $m[1] ] . ')';
      }
      if (substr($m[0], -1) === '+') {
          $this->paramNamesPath[ $m[1] ] = 1;

          return '(?P<' . $m[1] . '>.+)';
      }

      return '(?P<' . $m[1] . '>[^/]+)';
  }

  protected function selectController() {
    foreach ($this->map as $route => $controller) {
      if ($this->matches($this->getPathInfo(), $route)) {
        return $controller;
      }
    }
    return false;
  }

  protected function getRequestMethod() {
    return $_SERVER['REQUEST_METHOD'];
  }

  protected function getParams() {
    return $this->params;
  }

  protected function getFiles() {
    return [];
  }

  protected function canAccessRestrictedEndpoints() {
    return false;
  }

  protected function notFound() {
    $_GET['path_info'] = $this->getPathInfo();
    $controller = new BaseNotFoundController();
    return $controller;
  }

  public function run() {
    $method = $this->getRequestMethod();
    $controller_path = $this->selectController();

    if (false === $controller_path) {
      return $this->notFound();
    }

    $controller_name = array_pop(explode('/', $controller_path));
    switch ($method) {
      case 'GET':
          $controller_name .= 'Controller';
        break;
      case 'HEAD':
      case 'OPTIONS':
        $controller_name .= ucfirst(strtolower($method)) . 'Controller';
        $controller_path .= ucfirst(strtolower($method));
        $origin = idx(getallheaders(), 'Origin');
        $allow_headers = array_keys(getallheaders());
        $allow_headers[] = 'Access-Control-Allow-Origin';
        $headers = implode(', ', $allow_headers);
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 604800');
        header('Access-Control-Allow-Headers: ' . $headers);
        die;
        break;

      case 'POST':
      case 'PUT':
      case 'DELETE':
        $controller_path = str_replace($controller_name, '', $controller_path);
        $controller_path .= 'mutators/' . $controller_name .
          ucfirst(strtolower($method));

        $controller_name .= ucfirst(strtolower($method)) . 'Controller';
        break;
    }

    $controller_path = 'controllers/' . $controller_path . '.php';

    if (false === strpos($controller_name, 'Controller') ||
      !file_exists($controller_path)) {
      return $this->notFound();
    }

    require_once $controller_path;
    $controller = new $controller_name(
      $this->getPathInfo(),
      $this->getParams(),
      $this->getFiles(),
      $this->canAccessRestrictedEndpoints());

    return $controller;
  }
}

abstract class BaseEnum {
  protected $value;
  /**
   * Allows to reference a BaseEnum's value by calling:
   * AVeryLongEnumName::MY_VALUE()
   */
  public static function __callStatic($name, $args) {
    $const = static::class . '::' . $name;
    if (!defined($const)) {
      throw new InvalidArgumentException(
        sprintf(
          'Could not find enumeration %s in %s',
          $name,
          static::class));

      return null;
    } else {
      return new static($name);
    }
  }

  public final function __construct($key = '__default') {
    $this->setValue($key);
  }

  protected final function setValue($key) {
    if (static::isValid($key)) {
      $this->value = constant(static::class . '::' . $key);
    } else {
      throw new InvalidArgumentException(
        sprintf(
          'Could not find enumeration %s in %s',
          $key,
          get_class($this)));
    }
  }

  protected static final function isValid($key) {
    return defined(static::class . '::' . $key);
  }

  public static final function validValues() {
    $r = new ReflectionClass(get_called_class());
    return array_keys($r->getConstants());
  }

  public final function value() {
    return $this->value;
  }

  public final function __toString() {
    return (string)$this->value;
  }
}

class Base {
  public static function registerAutoloader() {
    spl_autoload_register(function ($class) {
      $map = [
        'Provider' => 'providers',
        'Model' => 'models',
        'Store' => 'storage',
        'Trait' => 'traits',
        'Enum' => 'const',
        'Const' => 'const',
        'Exception' => 'exceptions',
      ];

      // These classes are stored in lib/base or lib/queue, so no need to
      // autoload
      switch($class) {
        case 'BaseModel':
        case 'BaseEnum':
        case 'BaseStore':
        case 'BaseQueueStore':
        case 'BaseQueueFilesStore':
        case 'BaseQueueFileModel':
        return;
      }

      $pattern = '/^(\w+)(' . implode('|', array_keys($map)) . ')$/';

      $class_type = [];
      if (preg_match($pattern, $class, $class_type)) {
        $dir = array_pop($class_type);
        require $map[$dir] . DIRECTORY_SEPARATOR . str_replace($dir, '', $class) . '.php';
      }
    });
  }
}

<?php
abstract class BaseStore {
  protected
    $class,
    $db;

  public function __construct($collection = null, $class = null) {
    if (defined('static::COLLECTION') && defined('static::MODEL')) {
      $collection = static::COLLECTION;
      $class = static::MODEL;
    }

    if ($collection && $class) {
      $this->collection = $collection;
      $this->class = $class;
      $this->db = MongoInstance::get($collection);
    } else {
      throw new InvalidArgumentException('Collection or class not provided');
      return null;
    }
  }

  public function db() {
    return $this->db;
  }

  public function find($query) {
    $docs = $this->db->find($query);
    $class = $this->class;
    $res = [];

    foreach ($docs as $doc) {
      $res[] = new $class($doc);
    }

    return $res;
  }

  public function distinct($key, $query = []) {
    $docs = $this->db->distinct($key, $query);
    return !is_array($docs) ? [] : $docs;
  }

  public function findOne($query) {
    $doc = $this->db->findOne($query);
    $class = $this->class;
    return $doc ? new $class($doc) : null;
  }

  public function findById($id) {
    return $this->findOne(['_id' => mid($id)]);
  }

  public function count($query) {
    return $this->db->count($query);
  }

  protected function ensureType(BaseModel $item) {
    return class_exists($this->class) && is_a($item, $this->class);
  }

  public function remove(BaseModel $item) {
    if (!$this->ensureType($item)) {
      throw new Exception('Invalid object provided, expected ' . $this->class);
      return false;
    }

    if ($item == null) {
      return false;
    }

    try {
      if ($item->getID()) {
        $this->db->remove($item->document());
        return true;
      } else {
        return false;
      }
    } catch (MongoException $e) {
      l('MongoException:', $e->getMessage());
      return false;
    }
  }

  public function removeWhere($query = []) {
    try {
      $this->db->remove($query);
      return true;
    } catch (MongoException $e) {
      l('MongoException:', $e->getMessage());
      return false;
    }
  }

  protected function validateRequiredFields(BaseModel $item) {
    foreach ($item->schema() as $field => $type) {
      if ($field != '_id' &&
        $type === BaseModel::REQUIRED && !$item->has($field)) {
        return false;
      }
    }

    return true;
  }

  public function aggregate(BaseAggregation $aggregation) {
    return call_user_func_array(
      [$this->db, 'aggregate'],
      $aggregation->getPipeline());
  }

  public function mapReduce(
    MongoCode $map,
    MongoCode $reduce,
    array $query = null,
    array $config = null) {

    $options = [
      'mapreduce' => $this->collection,
      'map' => $map,
      'reduce' => $reduce,
      'out' => ['inline' => true]];

    if ($query) {
      $options['query'] = $query;
    }

    if ($config) {
      unset($options['mapreduce']);
      unset($options['map']);
      unset($options['reduce']);
      unset($options['query']);
      $options = array_merge($options, $config);
    }

    $res = MongoInstance::get()->command($options);

    if (idx($res, 'ok')) {
      return $res;
    } else {
      l('MapReduce error:', $res);
      return null;
    }
  }

  public function save(BaseModel &$item) {
    if (!$this->ensureType($item)) {
      throw new Exception('Invalid object provided, expected ' . $this->class);
      return false;
    }

    if (!$this->validateRequiredFields($item)) {
      throw new Exception('One or more required fields are missing.');
      return false;
    }

    if ($item == null) {
      return false;
    }

    try {
      if (!$item->getID()) {
        $id = new MongoID();
        $item->setID($id);
      }
      $this->db->save($item->document());

      return true;
    } catch (MongoException $e) {
      l('MongoException:', $e->getMessage());
      return false;
    }
    return true;
  }
}

class BaseAggregation {
  protected $pipeline;
  public function __construct() {
    $this->pipeline = [];
  }

  public function getPipeline() {
    return $this->pipeline;
  }

  public function project(array $spec) {
    if (!empty($spec)) {
      $this->pipeline[] = ['$project' => $spec];
    }
    return $this;
  }

  public function match(array $spec) {
    if (!empty($spec)) {
      $this->pipeline[] = ['$match' => $spec];
    }
    return $this;
  }

  public function limit($limit) {
    $this->pipeline[] = ['$limit' => $limit];
    return $this;
  }

  public function skip($skip) {
    $this->pipeline[] = ['$skip' => $skip];
    return $this;
  }

  public function unwind($field) {
    $this->pipeline[] = ['$unwind' => '$' . $field];
    return $this;
  }

  public function group(array $spec) {
    if (!empty($spec)) {
      $this->pipeline[] = ['$group' => $spec];
    }
    return $this;
  }

  public function sort(array $spec) {
    if (!empty($spec)) {
      $this->pipeline[] = ['$sort' => $spec];
    }
    return $this;
  }

  public function sum($value = 1) {
    if (!is_numeric($value)) {
      $value = '$' . $value;
    }
    return ['$sum' => $value];
  }

  public function __call($name, $args) {
    $field = array_pop($args);
    switch ($name) {
      case 'addToSet':
      case 'first':
      case 'last':
      case 'max':
      case 'min':
      case 'avg':
        return ['$' . $name => '$' . $field];
    }

    throw new RuntimeException('Method not found: ' . $name);
  }

  public function push($field) {
    if (is_array($field)) {
      foreach ($field as &$f) {
        $f = '$' . $f;
      }
    } else {
      $field = '$' . $field;
    }

    return ['$push' => $field];
  }
}


abstract class BaseModel {
  const
    FIELD = 'FIELD',
    REQUIRED = 'REQUIRED';

  protected
    $strict,
    $schema,
    $document;

  public function __construct($document = [], $strict = false) {
    $this->strict = !!$strict;
    $this->schema = $this->schema();

    $this->document = [];

    if (!is_array($this->schema)) {
      throw new Exception('Schema must be a key => value array.');
      return null;
    }

    $this->schema['_id'] = self::REQUIRED;

    if (is_array($document) && !empty($document)) {
      foreach ($document as $key => $value) {
        $this->set($key, $value);
      }
    }
  }

  abstract public function schema();

  public function __call($method, $args) {
    if (strpos($method, 'get') === 0) {
      $op = 'get';
    } elseif (strpos($method, 'set') === 0) {
      $op = 'set';
    } elseif (strpos($method, 'remove') === 0) {
      $op = 'remove';
    } elseif (strpos($method, 'has') === 0) {
      $op = 'has';
    } else {
      $e = sprintf('Method "%s" not found in %s', $method, get_called_class());
      throw new RuntimeException($e);
      return null;
    }

    $method = preg_replace('/^(get|set|remove|has)/i', '', $method);

    preg_match_all(
      '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!',
      $method,
      $matches);

    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ?
        strtolower($match) :
        lcfirst($match);
    }

    $field = implode('_', $ret);
    if ($this->strict && !idx($this->schema, $field)) {
      $e = sprintf(
        '"%s" is not a valid field for %s',
        $field,
        get_called_class());
      throw new InvalidArgumentException($e);
      return null;
    }

    $arg = array_pop($args);

    if (!method_exists($this, $op)) {
      $e = sprintf('%s::%s() is not defined', get_called_class(), $op);
      throw new Exception($e);
    }

    return $op == 'set' ? $this->set($field, $arg) : $this->$op($field);
  }

  private function overriddenMethod($prefix, $key) {
    $method = $prefix . preg_replace_callback(
      '/(?:^|_)(.?)/',
      function($matches) {
        return strtolower($matches[0]);
      },
      $key);

    return method_exists($this, $method) ? strtolower($method) : null;
  }

  public function getID() {return idx($this->document, '_id');}
  public function setID($value) {$this->document['_id'] = $value;}
  public final function get($key) {
    if (!is_string($key) || empty($key)) {
      throw new InvalidArgumentException('Invalid null key');
      return null;
    }

    $method = $this->overriddenMethod(__FUNCTION__, $key);
    if ($method) {
      return $this->$method();
    }

    if ($this->strict && !array_key_exists($key, $this->document)) {
      $e = sprintf(
        '\'%s\' is not a valid field for %s',
        $field,
        get_called_class());
      throw new InvalidArgumentException($e);
      return null;
    }

    return idx($this->document, $key);
  }

  public final function set($key, $value) {
    if (!is_string($key) || empty($key)) {
      throw new InvalidArgumentException('Invalid null key');
      return null;
    }

    $method = $this->overriddenMethod(__FUNCTION__, $key);
    if ($method) {
      return $this->$method($value);
    }

    if ($this->strict && !idx($this->schema, $key)) {
      $e = sprintf(
        '%s is not a valid field for %s',
        $field,
        get_called_class());
      throw new InvalidArgumentException($e);
      return;
    }

    $this->document[$key] = $value;
  }

  public function has($key) {
    if (!is_string($key) || empty($key)) {
      throw new InvalidArgumentException('Invalid null key');
      return null;
    }

    $method = $this->overriddenMethod(__FUNCTION__, $key);
    if ($method) {
      return $this->$method();
    }

    return array_key_exists($key, $this->document);
  }

  public final function remove($key) {
    if (!is_string($key) || empty($key)) {
      throw new InvalidArgumentException('Invalid null key');
      return null;
    }

    $method = $this->overriddenMethod(__FUNCTION__, $key);
    if ($method) {
      return $this->$method();
    }

    if ($this->strict &&
      (idx($this->schema, $key) == self::REQUIRED || $key == '_id')) {
      $e = sprintf(
        'Cannot remove %s required field %s',
        get_called_class(),
        $field);
      throw new InvalidArgumentException($e);
      return null;
    }

    unset($this->document[$key]);
  }

  public function document() {return $this->document;}
}

class Error {
  const
    SESSION_EXPIRED_OR_INVALID_SESSION = 1,
    NO_ACCESS_TOKEN = 2,
    FBUSER_NO_ADACCOUNT = 3,
    FBREQUEST_INVALID_RESPONSE = 4,
    OUR_FAULT = 5,
    INVALID_PARAMS = 6;
}

class MongoInstance {
  protected static $db = [];
  public static function get($collection = null, $with_collection = false) {
    if (strpos('mongodb://', $collection) !== false) {
      $db_url = explode('/', $collection);
      $collection = $with_collection ? array_pop($collection) : null;
      $db_url = implode('/', $db_url);
    } elseif (isset($_SERVER['MONGOHQ_URL'])) {
      $db_url = $_SERVER['MONGOHQ_URL'];
    } else {
      l('no mongoHQ url');
      return null;
    }

    if (idx(self::$db, $db_url)) {
      return $collection != null ?
        self::$db[$db_url]->selectCollection($collection) :
        self::$db[$db_url];
    }

    $dbname = array_pop(explode('/', $db_url));
    $db = new Mongo($db_url);
    self::$db[$db_url] = $db->selectDB($dbname);

    if ($collection) {
      return $collection != null ?
        self::$db[$db_url]->selectCollection($collection) :
        self::$db[$db_url];
    }
  }
}

class MongoFn {
  public static function get($file, $scope = []) {
    $code = file_get_contents('mongo_functions/' . $file . '.js');
    return new MongoCode($code, $scope);
  }
}
